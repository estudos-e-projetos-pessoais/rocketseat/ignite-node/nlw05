export default {
  root: '/',

  users: {
    index: '/users',
  },

  settings: {
    index: '/settings',
  },

  messages: {
    index: '/messages',
    user_id: '/messages/:user_id',
  },

  pages: {
    client: '/pages/client',
  },
};
