import { createConnection } from 'typeorm';
import { Socket } from 'socket.io';

import { http, io } from './http';
import './websockets/client';

(async () => {
  const PORT = 3333;

  console.log('before connection');
  await createConnection();
  console.log('after connection');

  /**
   * GET = Buscas
   * POST = Criação
   * PUT = Alteração
   * DELETE = Deletar
   * PATCH = Alterar uma informação espefícica (não indempotente)
   */

  http.listen(PORT, () => {
    console.log(`App is running on port ${PORT}`);
  });

  io.on('connection', (socket: Socket) => {
    console.log('ORIBOSS', socket.id);
  });
})();
