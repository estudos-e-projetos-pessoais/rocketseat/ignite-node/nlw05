import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  Generated,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { v4 as uuid } from 'uuid';

import User from './User';

@Entity('messages')
class Message {
  @PrimaryColumn({ type: 'uuid', unique: true })
  @Generated('uuid')
  id: string;

  @Column({ nullable: true })
  admin_id: string;

  @Column()
  user_id: string;

  @JoinColumn({ name: 'user_id' })
  @ManyToOne(() => User)
  user: User;

  @Column()
  text: string;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuid();
    }
  }
}

export default Message;
