import { createServer } from 'http';
import path from 'path';
import { renderFile } from 'ejs';

import 'reflect-metadata';
import { Server } from 'socket.io';
import express from 'express';

import router from './router';

/**
 * GET = Buscas
 * POST = Criação
 * PUT = Alteração
 * DELETE = Deletar
 * PATCH = Alterar uma informação espefícica (não indempotente)
 */

const app = express();
const http = createServer(app);
const io = new Server(http);

app.use(express.static(path.join(__dirname, '..', 'public')));
app.set('views', path.join(__dirname, '..', 'public'));
app.engine('html', renderFile);
app.set('view engine', 'html');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(router);

export { http, io };
