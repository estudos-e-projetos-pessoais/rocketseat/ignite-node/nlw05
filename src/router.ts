import { Router } from 'express';

import SettingsController from './controllers/SettingsController';
import UsersController from './controllers/UsersController';
import MessagesController from './controllers/MessagesController';

import routes from './config/routes';

const router = Router();

router.get(routes.root, (req, res) => res.render('html/client.html'));

// settings
const settingsController = new SettingsController();
router.post(routes.settings.index, settingsController.create.bind(settingsController));

// users
const usersController = new UsersController();
router.post(routes.users.index, usersController.create.bind(usersController));

// messages
const messagesController = new MessagesController();
router.post(routes.messages.index, messagesController.create.bind(messagesController));
router.get(routes.messages.user_id, messagesController.showByUser.bind(messagesController));

export default router;
