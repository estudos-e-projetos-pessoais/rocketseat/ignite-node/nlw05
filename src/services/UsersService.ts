import { getCustomRepository } from 'typeorm';

import User from '../models/User';
import UsersRepository from '../repositories/UsersRepository';

interface UsersCreateInterface {
  email: string;
}

class UsersService {
  async create({ email }: UsersCreateInterface): Promise<User> {
    const usersRepository = getCustomRepository(UsersRepository);

    let user = await usersRepository.findOne({ email });

    if (!user) {
      user = usersRepository.create({ email });
      await usersRepository.save(user);
    }

    return user;
  }
}

export default UsersService;
