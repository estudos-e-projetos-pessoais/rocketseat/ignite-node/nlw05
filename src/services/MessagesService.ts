import { getCustomRepository } from 'typeorm';

import Message from '../models/Message';
import MessagesRepository from '../repositories/MessagesRepository';

interface MessagesCreateInterface {
  user_id: string;
  text: string;
  admin_id?: string;
}

class MessagesService {
  // repository: MessagesRepository;

  // constructor() {
  //   console.log('constructor');
  //   this.repository = getCustomRepository(MessagesRepository);
  // }

  async create({ user_id, text, admin_id }: MessagesCreateInterface): Promise<Message> {
    const messagesRepository = getCustomRepository(MessagesRepository);

    const message = messagesRepository.create({
      admin_id,
      text,
      user_id,
    });

    await messagesRepository.save(message);

    return message;
  }

  async listByUser(user_id: string): Promise<Array<Message>> {
    const messagesRepository = getCustomRepository(MessagesRepository);

    const messages = await messagesRepository.find({
      where: { user_id },
      relations: ['user'],
    });

    return messages;
  }
}

export default MessagesService;
