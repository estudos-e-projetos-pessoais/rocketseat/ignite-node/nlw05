import { getCustomRepository } from 'typeorm';

import Setting from '../models/Setting';
import SettingsRepository from '../repositories/SettingsRepository';

interface SettingsCreateInterface {
  chat: boolean;
  username: string;
}

class SettingsService {
  async create({ chat, username }: SettingsCreateInterface): Promise<Setting> {
    const settingsRepository = getCustomRepository(SettingsRepository);

    const userAlreadyExists = Boolean(await settingsRepository.findOne({ username }));

    if (userAlreadyExists) {
      throw new Error('User already exists');
    }

    const settings = settingsRepository.create({
      chat,
      username,
    });

    await settingsRepository.save(settings);

    return settings;
  }
}

export default SettingsService;
