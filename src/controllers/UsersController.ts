import { Request, Response } from 'express';
import UsersService from '../services/UsersService';

class UsersController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const usersService = new UsersService();
      const user = await usersService.create(req.body);
      return res.json(user);
    }
    catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }
}

export default UsersController;
