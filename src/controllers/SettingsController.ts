import { Request, Response } from 'express';
import SettingsService from '../services/SettingsService';

class SettingsController {
  async create(req: Request, res: Response): Promise<Response> {
    try {
      const settingsService = new SettingsService();
      const settings = await settingsService.create(req.body);
      return res.json(settings);
    }
    catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }
}

export default SettingsController;
