import { Request, Response } from 'express';
import MessagesService from '../services/MessagesService';

class MessagesController {
  service: MessagesService;

  constructor() {
    this.service = new MessagesService();
  }

  async create(req: Request, res: Response): Promise<Response> {
    try {
      const message = await this.service.create(req.body);
      return res.json(message);
    }
    catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }

  // localhost:3333/messages/:user_id
  async showByUser(req: Request, res: Response): Promise<Response> {
    try {
      const { user_id } = req.params;
      const messages = await this.service.listByUser(user_id);
      return res.json(messages);
    }
    catch (err) {
      return res.status(400).json({ message: err.message });
    }
  }
}

export default MessagesController;
